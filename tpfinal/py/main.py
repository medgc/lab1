#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 23 22:04:44 2019

@author: fborghesi
"""


# imports globales
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os
import sys
from sklearn.impute import SimpleImputer
from bayes_opt import BayesianOptimization


# mis modulos
#sys.path.append('py/')
from dataset import Dataset
import util

# constantes
DATASETS_DIR = os.getenv('HOME') + '/workspace/medgc/lab1/Austral2019/datasets'
FNAME_201902 = os.path.join(DATASETS_DIR, '201902.txt')
FNAME_201904 = os.path.join(DATASETS_DIR, '201904.txt')
SEEDS = [214219, 214237, 214243, 214259, 214283]
TRAINING_PROB = 0.7
CLASS_ATTR_NAME = 'clase_ternaria'


# load train-test data
imputer = SimpleImputer(missing_values=np.nan, strategy='constant', fill_value=0)
ds201902 = Dataset.load(FNAME_201902)
ds201902.impute(imputer)
dsTrain, dsTest = ds201902.split(TRAINING_PROB, SEEDS[0])

# load validation data
dsValidation = Dataset.load(FNAME_201904)
dsValidation.impute(imputer);
real_ids = dsValidation.get_data().loc[dsValidation.get_data().clase_ternaria == 'BAJA+2', 'numero_de_cliente'].tolist()

# entreno el modelo
"""
see https://github.com/fmfn/BayesianOptimization
pbounds = {'pmaxdepth': (2, 40), 'pminbucket': (0, 50), 'pminsplit': (8, 100)}
optimizer = BayesianOptimization(
    f = create_tree,
    pbounds = pbounds,
    random_state= 1
)
optimizer.maximize(init_points = 2, n_iter = 3)
"""
model = util.create_tree(dsTrain.get_data(), CLASS_ATTR_NAME, SEEDS[0])

# prueba del modelo con el set de training
gain = util.compute_gain(run_model(model, dsTrain.get_data()), real_ids)
print(gain)

# prueba del modelo con el set de testing
gain = util.compute_gain(run_model(model, dsTest.get_data()), real_ids)
print(gain)

# prueba del modelo con el set futuro
gain = util.compute_gain(run_model(model, dsValidation.get_data()), real_ids)
print(gain)
