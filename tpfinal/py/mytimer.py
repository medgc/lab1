# -*- coding: utf-8 -*-
"""
Created on Wed Oct 2 11:00:44 2019

@author: fborghesi
"""

import timeit

class MyTimer:
    "Timer class for timing events."

    def __init__(self):
        self._timer_start_time = timeit.default_timer()
        self.time = 0

    def stop(self): 
        self.time = timeit.default_timer() - self._timer_start_time
        print('%.2f seconds elapsed.' % self.time)