# -*- coding: utf-8 -*-
"""
Created on Wed Oct 2 11:30:44 2019

@author: fborghesi
"""

import pandas as pd
import numpy as np
import time

class Dataset:
    """Clase de utilidad para la manipulación de datasets.

    :param data: Panda's Dataframe a reutilizar.
    """
    def __init__(self, data=False):
        self.data = data

    @classmethod
    def load(cls, file_name, separator='\t'):
        """Metodo factoria que carga los datos del archivo indicado.

        :param file_name: Nombre del archivo CSV a procesar.
        :param separator: Separador de columnas en el archivo.

        :return Dataset: Instancia del dataset con los datos.
        """
        data = pd.read_csv(file_name, sep=separator)
        print("Loaded %s, shape: %s" % (file_name, data.shape))
        return cls(data)

    def impute(self, imputer):
        """Imputa los valores en las columnas del dataframe utilizando el SimpleImputer recibido.

        :param imputer SimpleImputer: El Imputer a utilizar, previamente configurado.
        """
        df = pd.DataFrame(imputer.fit_transform(self.data))
        df.columns = self.data.columns
        df.index = self.data.index
        self.data = df

    def get_data(self):
        """
        Devuelve el dataframe contenido.
        :return: el dataframe con los datos.
        """
        return self.data

    def split(self,  train_prob = 0.7, seed = -1):
        """Particiona este dataset dos sub-conjuntos (también datasets), 
        utilizando números aleatorios generados a partir de la semilla recibida. 
        El tamaño de los sub-conjuntos es determinado a partir del parámetro 
        train_prob.

        :param train_prob: El porcentaje de registros de training (se asume
        que test_prob = 1 - train_prob).
        :param seed: La semilla a utilizar para inicializar la generación de
        valores random.
        
        :return tuple: los datasets de training y test, ambos con la misma
        estructura que la instancia contenida.
        """
        length = self.data.shape[0]

        seed = int(round(time.time() * 1000)) if seed == -1 else seed

        train_len = int(length * train_prob)
        random = np.random.RandomState(seed = seed).permutation(length)
        train_rows = random[:train_len]
        test_rows = random[train_len:]
        
        return Dataset(self.data.iloc[train_rows]), Dataset(self.data.iloc[test_rows])


