#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 2 14:27:44 2019

@author: fborghesi
"""

import pandas as pd
import numpy as np
from sklearn import tree
from mytimer import MyTimer


TRUE_POSITIVE_GAIN = 19500.0
FALSE_POSITIVE_LOSS = 500.0

TREE_SEED_DEFAULT = 0
TREE_MAXDEPTH_DEFAULT = 10
TREE_MINBUCKET_DEFAULT = 20
TREE_MINSPLIT_DEFAULT = 55



PREDICT_CLASS_ATTR = 'clase_ternaria'
PREDICT_ID_ATTR = 'numero_de_cliente'
PREDICT_CLASS_SUCCESS = 'BAJA+2'

def run_model(model, dataframe,
    id_attr = PREDICT_ID_ATTR,
    class_attr = PREDICT_CLASS_ATTR,
    class_success = PREDICT_CLASS_SUCCESS):

    data = dataframe.loc[:, dataframe.columns != class_attr]
    prediction = model.predict(data)
    result = dataframe.loc[prediction == class_success, id_attr].tolist()

    return result;

def compute_gain(model_ids, real_ids, true_positive_gain = TRUE_POSITIVE_GAIN, 
false_positive_loss = FALSE_POSITIVE_LOSS):
    """Computa la ganancia en terminos de $$$.

    Args:
    model_ids: array de identificadores pronosticados por el modelo.
    real_ids: array de identificadores del conjunto de validacion.
    true_positive_gain: float con el monto ganado con cada pronostico acertado.
    false_positive_lass: float con el monto perdido con cada pronostico errado.

    Returns:
    Float con el resultado calculado.
    """

    # cuento los pronosticos acertados
    found = [id for id in model_ids if id in real_ids]
    true_pos_count = len(found)

    # calculo los errados y devuelvo el calculo de $$$
    false_pos_count = len(model_ids) - true_pos_count
    return true_pos_count * true_positive_gain - false_pos_count *false_positive_loss



def create_tree(ds, class_attr_name,
    psemilla = TREE_SEED_DEFAULT,
    pmaxdepth = TREE_MAXDEPTH_DEFAULT,
    pminbucket = TREE_MINBUCKET_DEFAULT,
    pminsplit = TREE_MINSPLIT_DEFAULT):

    # datos de entrenamiento
    X = ds.loc[:,ds.columns != class_attr_name]
    y = ds.loc[:,class_attr_name]
    
    # entreno el arbol
    t = MyTimer()
    model = tree.DecisionTreeClassifier(
            criterion = 'gini',
            max_depth = pmaxdepth,
            min_samples_leaf = pminbucket,
            min_samples_split = pminsplit
            )
    model.fit(X, y)
    t.stop()

    return model




