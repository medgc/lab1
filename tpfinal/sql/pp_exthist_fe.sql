

-- agrego el campo foto_mes_ant y lo calculo
ALTER TABLE pp_exthist ADD foto_mes_ant INTEGER;
UPDATE pp_exthist SET 
  foto_mes_ant = to_char(make_date(substr(foto_mes::TEXT, 1, 4)::INTEGER, substr(foto_mes::TEXT, 5, 2)::INTEGER, 1) - '1 month'::INTERVAL, 'YYYYmm')::INTEGER;

ALTER TABLE pp_exthist ADD mv_msaldopesos_tnd NUMERIC;
ALTER TABLE pp_exthist ADD mv_msaldodolares_tnd NUMERIC;
ALTER TABLE pp_exthist ADD mv_mconsumospesos_tnd NUMERIC;
ALTER TABLE pp_exthist ADD mv_mconsumosdolares_tnd NUMERIC;

UPDATE pp_exthist SET 
   mv_msaldopesos_tnd = mv_msaldopesos / PP_ANT.mv_msaldopesos,
   mv_msaldodolares_tnd = mv_msaldodolares / PP_ANT.mv_msaldodolares,
   mv_mconsumospesos_tnd = mv_mconsumospesos / PP_ANT.mv_mconsumospesos,
   mv_mconsumosdolares_tnd = mv_mconsumosdolares / PP_ANT.mv_mconsumosdolares,
FROM 
  LEFT JOIN pp_exthist PP_ANT ON PP_ANT.numero_de_cliente = pp_exthist.numero_de_cliente AND PP_ANT.foto_mes = pp_exthist.foto_mes_ant



